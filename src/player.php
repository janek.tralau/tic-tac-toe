<?php

class Player{

    private $char;
    private $winCount;
    private $lostCount;

    function __construct($char) {
        
        $this->char = $char;
        $this->winCount = 0;
        $this->lostCount = 0;

    }

    function addWinCount(){
        $this->winCount++;
    }

    function addLostCount(){
        $this->lostCount++;
    }

    function getWinCount(){
        return $this->winCount;
    }

    function getLostCount(){
        return $this->lostCount;
    }

    function getChar(){
        return $this->char;
    }
   

}

?>