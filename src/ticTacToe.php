<?php

class TicTacToe{

    private $gameField;
    private $currentPlayer;
    private $players;
    private $currentPlayerWon = false;

    function __construct() {
        
        $this->gameField = new GameField();
        $this->players = array(new Player("X"), new Player("O"));
        $this->currentPlayer = $this->players[0];

    }

    function draw(){
        
        $this->gameField->draw( $this->currentPlayer);
        
        if($this->currentPlayerWon === true)
        {
            echo 'Player "'. $this->currentPlayer->getChar().'" has won.';
        }

    }

    function addMove($i, $j, $char){

        if($this->currentPlayerWon === false)
        {
            $addMoveSuccessful = $this->gameField->addMove($i, $j, $char);

            if($addMoveSuccessful === true)
            {
                $this->currentPlayerWon = $this->checkForWin();
            
                if($this->currentPlayerWon === false)
                {
                        $this->switchPlayer();
                }
            }
        }
        
    }

    
    function switchPlayer(){
      
        if( $this->players[0]->getChar() ===  $this->currentPlayer->getChar())
        {
            $this->currentPlayer = $this->players[1];
        }
        else 
        {
            $this->currentPlayer = $this->players[0];
        }
    }

    function getCurrentPlayer(){
        return  $this->currentPlayer;
    }

    function checkForWin(){

        $field = $this->gameField->getField();
        
        $playerChar = $this->currentPlayer->getChar();

        $playerWin = false;

        for ($i=0; $i < count($field) && !$playerWin; $i++) 
        { 
            // check playing field vertical ⬇
            if($field[$i][0] == $playerChar && $field[$i][1] == $playerChar && $field[$i][2] == $playerChar)
            {            
                $playerWin = true;
            }

            // check playing field horizontally ➡
            if($field[0][$i] == $playerChar && $field[1][$i] == $playerChar && $field[2][$i] == $playerChar)
            {            
                $playerWin = true;
            }
        }

     
        if( $playerWin === false )
        {
            // check the playing field diagonally from top left to bottom right ↘
            if($field[0][0] == $playerChar && $field[1][1] == $playerChar && $field[2][2] == $playerChar)
            {
                $playerWin = true;
            }
            // check the playing field diagonally from top right to bottom left  ↙
            else if($field[0][2] == $playerChar && $field[1][1] == $playerChar && $field[2][0] == $playerChar)
            {                
                $playerWin = true;
            }
        }

        return $playerWin;
    }
}

?>