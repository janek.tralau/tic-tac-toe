<?php

class GameField{

    private $field;

    function __construct() {
        
        $this->field = array( array("","",""), array("","",""), array("","",""));

    }

    function draw($player){

        echo '<table class="tic">'."\n";

        // goes through the field array and creates a table entry
        // if player is set a span tag with the player character is added, 
        // else a  input tag with the player character as value and add the current row and current column in the name attribute
        for ($i = 0; $i < count($this->field); $i++) 
        {     
            echo "\t".'<tr>'."\n";

            for ($j= 0; $j < count($this->field[$i]); $j++) 
            { 
                
                $cell = $this->field[$i][$j];
                $playerChar = $player->getChar();
              

                if( $cell === "")
                {
                    echo "\t\t".'<td><input type="submit" class="field" name="cell-'.$i.'-'.$j.'" value="'.$playerChar.'" /></td>'."\n";
                }
                else 
                {
                    echo "\t\t".'<td><span class="color'.$cell.'">'.$cell.'</span></td>'."\n";
                }
               
            }

            echo "\t".'</tr>'."\n";
        }

        echo '</table>'."\n";
    }

    function addMove($i, $j, $char){
        
        if($this->field[$i][$j] === "")
        {
            $this->field[$i][$j] = $char;
            return true;
        }
        else
        {
            return false;
        }
        
    
    }

    function getField(){
        return $this->field;
    }

}

?>