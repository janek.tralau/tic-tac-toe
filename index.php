<?php
    session_start();

    define ('BASEPATH', realpath(dirname(__FILE__)));
    require_once (BASEPATH.DIRECTORY_SEPARATOR.'vendor' .DIRECTORY_SEPARATOR.'autoload.php');

    // check the session "ticTacToe" exist
    // if not, create a new TicTacToe game and the session "ticTacToe"
    if(isset($_SESSION['ticTacToe']))
    {
        $ticTacToe = unserialize($_SESSION['ticTacToe']);  //convert string to TicTacToe Object

        // check which cell was clicked and add move
        for ($i = 0; $i < 3; $i++) 
        {   
            for ($j= 0; $j < 3; $j++) 
            { 
                
                $cell = filter_input(INPUT_GET, 'cell-'.$i.'-'.$j, FILTER_SANITIZE_SPECIAL_CHARS); // if it does not exist, will null returned
                
                if($cell !== null)
                {
                    $ticTacToe->addMove( $i, $j , $cell);
                    $_SESSION['ticTacToe'] = serialize( $ticTacToe);
                }
            }
        }
    }
    else
    {
        $ticTacToe = new TicTacToe();
        $_SESSION['ticTacToe'] = serialize( $ticTacToe); // convert TicTacToe Object to string
    }

?>


<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Tic-Tac-Toe</title>
    <meta name="description" content="Tic-Tac-Toe-Game. Here is a short description for the page. This text is displayed e. g. in search engine result listings.">
    <style>
        table.tic td {
            border: 1px solid #333; /* grey cell borders */
            width: 8rem;
            height: 8rem;
            vertical-align: middle;
            text-align: center;
            font-size: 4rem;
            font-family: Arial;
        }
        table { margin-bottom: 2rem; }
        input.field {
            border: 0;
            background-color: white;
            color: white; /* make the value invisible (white) */
            height: 8rem;
            width: 8rem;
            font-family: Arial;
            font-size: 4rem;
            font-weight: normal;
            cursor: pointer;
        }
        input.field:hover {
            border: 0;
            color: #c81657; /* red on hover */
        }
        .colorX { color: #e77; } /* X is light red */
        .colorO { color: #77e; } /* O is light blue */
        table.tic { border-collapse: collapse; }
    </style>
</head>
<body>
    <section>
        <h1>Tic-Tac-Toe</h1>
        <article id="mainContent">
            <h2>Your free browsergame!</h2>
            <p>Type your game instructions here...</p>
            <p>Current player is <?php echo $ticTacToe->getCurrentPlayer()->getChar(); ?></p>
            <form method="get" action="index.php">

                <?php $ticTacToe->draw(); ?>

            </form>
        </article>
    </section>
</body>
</html>